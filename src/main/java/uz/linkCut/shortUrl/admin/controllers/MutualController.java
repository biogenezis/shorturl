package uz.linkCut.shortUrl.admin.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import uz.linkCut.shortUrl.admin.services.MainService;
import uz.linkCut.shortUrl.admin.crud.CrudMongoDb;
import uz.linkCut.shortUrl.admin.models.LinkProperties;


@RestController
@RequestMapping("/admin/")
public class MutualController {

    @Autowired
    CrudMongoDb crud;
    @Autowired
    MainService service;
    @Autowired
    LinkProperties request;

    @PostMapping("/links")
    public LinkProperties cutLinkB(@RequestBody LinkProperties linkProperties) {
        return service.createFields(linkProperties);
    }
}

package uz.linkCut.shortUrl.admin.crud;
import org.springframework.stereotype.Component;
import org.springframework.data.mongodb.repository.MongoRepository;
import uz.linkCut.shortUrl.admin.models.LinkProperties;

import java.util.List;



@Component
public interface CrudMongoDb extends MongoRepository<LinkProperties, String> {
    // поиск по укороченной ссылке в бд
    List<LinkProperties> findByCutUri(String cutUri);
}
package uz.linkCut.shortUrl.admin.models;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.stereotype.Component;


@Component
@Document(collection = "links")
public class LinkProperties {

    // Поля, акцессоры и мутаторы документа

    @Id
    private String id;
    private String defUri;
    private String cutUri;
    private String iosUri;
    private String androidUri;
    private boolean available;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public String getDefUri() {
        return defUri;
    }

    public void setDefUri(String defUri) {
        this.defUri = defUri;
    }

    public String getCutUri() {
        return cutUri;
    }

    public void setCutUri(String cutUri) {
        this.cutUri = cutUri;
    }

    public String getIosUri() {
        return iosUri;
    }

    public void setIosUri(String iosUri) {
        this.iosUri = iosUri;
    }

    public String getAndroidUri() {
        return androidUri;
    }

    public void setAndroidUri(String androidUri) {
        this.androidUri = androidUri;
    }

}



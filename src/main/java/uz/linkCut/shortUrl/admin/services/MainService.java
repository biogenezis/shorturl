package uz.linkCut.shortUrl.admin.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.linkCut.shortUrl.admin.crud.CrudMongoDb;
import uz.linkCut.shortUrl.admin.models.LinkProperties;

import java.util.List;
import java.util.Objects;
import java.util.UUID;


@Service
public class MainService {

    @Autowired
    private CrudMongoDb crud;

    // проверка совпадения ссылки в бд, возвращает ссылку, если совпадение было найдено

    public LinkProperties compareLinks(String cutUri) {
        return crud.findByCutUri(cutUri)
                .stream()
                .findFirst()
                .orElse(null);
    }

    public String redirect(String cutUri, String userAgent) {
        LinkProperties uriSet = compareLinks(cutUri);
        String redirectTo = "not found";

        if (Objects.nonNull(uriSet)) {
            if (userAgent.contains("Macintosh")) {
                redirectTo = uriSet.getIosUri();
            } else if (userAgent.contains("Android")) {
                redirectTo = uriSet.getAndroidUri();
            } else {
                redirectTo = uriSet.getDefUri();
            }
        }

        return String.format("redirect: %s", redirectTo);
    }

//    генератор рандомной ссылки

    public String generateCutLink() {
        // https://cut.uri.uz/
        String id1 = UUID.randomUUID().toString();
        id1 = id1.replace("-", "");
        id1 = id1.substring(20);
        return id1;

    }

//    метод создания полей и сохранения документа




    public LinkProperties createFields(LinkProperties linkProperties)
    {    linkProperties.setCutUri(generateCutLink());
        return crud.save(linkProperties);
    }

}



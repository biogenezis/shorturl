package uz.linkCut.shortUrl.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import javax.servlet.http.HttpServletRequest;
import uz.linkCut.shortUrl.admin.services.MainService;
import uz.linkCut.shortUrl.admin.crud.CrudMongoDb;
import uz.linkCut.shortUrl.admin.models.LinkProperties;


@Controller
@RequestMapping("/")
public class MainController {
    @Autowired
    CrudMongoDb crud;
    @Autowired
    MainService service;

    @GetMapping("/{link}")
    public String redirect(@PathVariable String link, HttpServletRequest request) {
        return service.redirect(link, request.getHeader("User-Agent"));
    }

}
